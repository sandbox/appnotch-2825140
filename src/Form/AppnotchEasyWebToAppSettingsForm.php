<?php

namespace Drupal\appnotch_easy_web_to_app\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * {@inheritdoc}
 */
class AppnotchEasyWebToAppSettingsForm extends ConfigFormBase {
  public $appnotchEndpoint = "http://qa.appnotch.com/partner/wordpress/home/";

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'appnotch_easy_web_to_app_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    global $base_url;
    $config        = $this->config('appnotch_easy_web_to_app.settings');
    $url           = $config->get('url');
    $email         = $config->get('email');
    $app_name      = $config->get('app_name');
    $generated_url = $config->get('generated_url');

    $form['appnotch_easy_web_to_app_app_name'] = array(
      '#type'          => 'textfield',
      '#title'         => $this->t('App Name'),
      '#default_value' => $app_name ? $app_name : "My App",
        // '#required'      => true,.
      '#disabled'      => $generated_url ? TRUE : FALSE,
    );

    $form['appnotch_easy_web_to_app_email'] = array(
      '#type'          => 'email',
      '#title'         => $this->t('Admin Email'),
      '#default_value' => $email ? $email : $this->config('system.site')->get('mail'),
      '#required'      => TRUE,
      '#disabled'      => $generated_url ? TRUE : FALSE,

    );

    $form['appnotch_easy_web_to_app_url'] = array(
      '#type'          => 'url',
      '#title'         => $this->t('Url'),
      '#default_value' => $url ? $url : $base_url,
      '#required'      => TRUE,
      '#disabled'      => $generated_url ? TRUE : FALSE,

    );

    if ($generated_url) {

      $form['appnotch_easy_web_to_app_generated_url'] = array(
        '#type'   => 'item',
        '#title'  => $this->t('Generated URL'),
        '#markup' => '<a href="' . $generated_url . '" target="_blank">Click here</a>',

      );
    }

    // Return parent::buildForm($form, $form_state);.
    $form                     = parent::buildForm($form, $form_state);
    $form['actions']['#type'] = 'action';
    if ($generated_url) {
      $form['actions']['submit'] = array();
    }
    else {
      $form['actions']['submit'] = array(
        '#type'        => 'submit',
        '#value'       => $this->t('Save Configuration'),
        '#button_type' => 'primary',
      );
    }

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state_values = $form_state->getValues();
    $config            = $this->config('appnotch_easy_web_to_app.settings');

    $query = http_build_query(
        array(
          "email"    => $form_state_values['appnotch_easy_web_to_app_email'],
          "url"      => $form_state_values['appnotch_easy_web_to_app_url'],
          "app_name" => $form_state_values['appnotch_easy_web_to_app_app_name'],
        ));

    $config->set('email', $form_state_values['appnotch_easy_web_to_app_email'])
      ->set('url', $form_state_values['appnotch_easy_web_to_app_url'])
      ->set('app_name', $form_state_values['appnotch_easy_web_to_app_app_name'])
      ->set('generated_url', $this->appnotchEndpoint . "?" . $query)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['appnotch_easy_web_to_app.settings'];
  }

}
